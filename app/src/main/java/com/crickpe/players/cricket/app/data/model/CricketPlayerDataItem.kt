package com.crickpe.players.cricket.app.data.model

data class CricketPlayerDataItem(
    val name: String,
    val title: String,
    val imageId: Int
)