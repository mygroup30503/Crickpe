package com.crickpe.players.cricket.app.data.repository

import com.crickpe.players.cricket.app.data.model.CricketPlayerDataItem
import com.crickpe.players.cricket.app.data.storage.CrickpeStorage
import com.crickpe.players.cricket.app.domain.repository.CrickpeGetterRepository

class CrickpeGetterRepositoryImpl(private val storage: CrickpeStorage): CrickpeGetterRepository {
    override fun gimmePlayerByIndex(index: Int): CricketPlayerDataItem? {
        return storage.getCricketPlayerByIndex(index)
    }

    override fun gimmePlayerList(): List<CricketPlayerDataItem> {
        return storage.getAllCrickpe()
    }

}