package com.crickpe.players.cricket.app.data.storage

import com.crickpe.players.cricket.app.R
import com.crickpe.players.cricket.app.data.model.CricketPlayerDataItem

class CrickpeStorage {
    private val list = listOf(
        CricketPlayerDataItem(
            "Rashid Khan",
            "Despite not relying on significant spin, Rashid Khan’s leggies have evolved into" +
                    " one of the most potent weapons in white-ball cricket. His red-ball career has " +
                    "been equally impressive, with Afghanistan recently earning Test status, and " +
                    "Rashid showcasing remarkable performance right from the start. Rashid " +
                    "Khan’s accuracy and deceptive googly have led him to thrive in various " +
                    "short-form leagues, boasting an outstanding record in over 150 white-ball " +
                    "matches for Afghanistan. Additionally, he showcases his batting prowess by " +
                    "swinging hard and contributing valuable runs whenever the opportunity arises.",
            R.drawable.player_1
        ),
        CricketPlayerDataItem(
            "Babar Azam",
            "Initially making a name for himself as a star in Pakistan’s white-ball formats. Babar Azam has grown into one of the nation’s greatest-ever batters. His remarkable statistics in Tests, ODIs, and T20s showcase his exceptional talent and dedication to the sport. Despite being part of a subpar Pakistan batting line-up, Azam has shouldered significant pressure and consistently delivered stellar performances.\u2028In the company of the Fab Four, Babar Azam has not only held his own but also surpassed several members of that illustrious quartet in recent years.",
            R.drawable.player_2
        ),
        CricketPlayerDataItem(
            "Suryakumar Yadav",
            "Suryakumar Yadav’s reputation as a T20 genius is well-established, especially during his time with the Mumbai Indians in the IPL. However, ranking him becomes a challenge due to the limited number of ODIs under his belt and his absence from Test cricket. Despite being the best T20 batsman globally, he has only played 20 ODIs, and his Test debut is yet to happen. SKY’s journey to the world stage has taken place in his 30s, partly due to being a late bloomer and partly because India delayed his international call-up. Nevertheless, his performances in the IPL and his brief stint in international cricket showcase his true potential as a 360-hitter.",
            R.drawable.player_3
        ),
        CricketPlayerDataItem(
            "Ben Stokes",
            "Who else comes close to Ben Stokes? While he may not hold the title of the best batsman or bowler in the world, nor boast the top all-rounder statistics, Stokes emerges as the go-to man when the stakes are highest. With bat or ball in hand, he shines brightest under pressure. From the exhilarating World Cup triumphs to the jaw-dropping heroics at Headingley in 2019, Stokes has etched his name in cricketing history. Beyond these iconic moments, his consistent match-turning performances for his team have solidified his reputation as a game-changer.",
            R.drawable.player_4
        ),
        CricketPlayerDataItem(
            "Rashid Khan",
            "Despite not relying on significant spin, Rashid Khan’s leggies have evolved into one of the most potent weapons in white-ball cricket. His red-ball career has been equally impressive, with Afghanistan recently earning Test status, and Rashid showcasing remarkable performance right from the start.\u2028Rashid Khan’s accuracy and deceptive googly have led him to thrive in various short-form leagues, boasting an outstanding record in over 150 white-ball matches for Afghanistan. Additionally, he showcases his batting prowess by swinging hard and contributing valuable runs whenever the opportunity arises." +
                    "",
            R.drawable.player_5
        ),
        CricketPlayerDataItem(
            "Shakib Al Hasan",
            "Shakib Al Hasan’s career has seen him as one of cricket’s most underrated players. Using a traditional measure of an all-rounder, his batting average surpasses his bowling average in Tests, ODIs, and T20s. Impressive ICC rankings place him as the number one all-rounder in T20Is and ODIs, and third in Tests.\u2028Amidst controversies, Shakib’s true value as a player will likely be recognized only after his retirement. Nevertheless, he stands out as Bangladesh’s standout performer.",
            R.drawable.player_6
        ),
        CricketPlayerDataItem(
            "Jasprit Bumrah",
            "Jasprit Bumrah, when in top form, stands as the epitome of a complete fast bowler in the world of cricket. With his distinctive, jerky action and a short, brisk run-up, he becomes an enigma that perplexes batters. The sheer variety in his bowling arsenal allows him to torment batsmen in numerous ways, making him a force to be reckoned with on the cricket field.\u2028Bumrah’s red-ball prowess lies in his ability to swing and seam the ball, creating awkward angles that trouble batsmen. When equipped with the white ball, he unleashes a hostile bouncer to complement his variations and precision in delivering pinpoint Yorkers. Though unorthodox, Bumrah’s diverse skillset positions him as the quintessential modern pace bowler.",
            R.drawable.player_0
        ),
        CricketPlayerDataItem(
            "Joe Root",
            "Joe Root’s averages may not be as impressive as those of some other top batters globally, but this can largely be attributed to him playing a significant portion of his cricket on demanding English pitches.\u2028One of the best players of his generation, Root is perhaps England’s all-time best hitter. While his exclusion from England’s white-ball squads may be considered a disadvantage, it speaks more to the quantity of batting talent present in the shorter forms than it does to his abilities in those games.",
            R.drawable.player_7
        ),
        CricketPlayerDataItem(
            "Babar Azam",
            "Initially making a name for himself as a star in Pakistan’s white-ball formats. Babar Azam has grown into one of the nation’s greatest-ever batters. His remarkable statistics in Tests, ODIs, and T20s showcase his exceptional talent and dedication to the sport. Despite being part of a subpar Pakistan batting line-up, Azam has shouldered significant pressure and consistently delivered stellar performances.\u2028In the company of the Fab Four, Babar Azam has not only held his own but also surpassed several members of that illustrious quartet in recent years.",
            R.drawable.player_8
        ),
        CricketPlayerDataItem(
            "Kagiso Rabada",
            "Much like Afridi, Kagiso Rabada burst onto the international cricket scene at a tender age and has continued to shine ever since. His versatility and prowess have been evident across all three formats, where his pace and bounce make him a formidable force even on surfaces that offer little assistance to fast bowlers. Following a golden era of South African fast bowling, led by the likes of Dale Steyn and Morne Morkel, Kagiso Rabada has stepped up as the leader of the attack. His outstanding performance across the board, including a historically great Test match strike rate, cements his status as a true asset to the South African cricket team.",
            R.drawable.player_9
        ),
        CricketPlayerDataItem(
            "Steve Smith",
            "Steve Smith’s record is a testament to his brilliance on the cricket field. With an average of over 60 in Test cricket, he stands as a true stalwart of the longest format. While he may not be as prolific in white-ball cricket, Smith boasts an impressive record in ODIs. After a remarkable average of over 110 during the 2019 Ashes. The Sydney native has shown a slightly lower output in Test cricket since then. Nonetheless, he continues to be counted among the elite batters in the world’s longest format. Steve Smith’s success transcends all types of playing surfaces. His ability to perform exceptionally well in various conditions solidifies his place as one of the greatest batters of the 21st century. As he continues to etch his name in cricketing history.",
            R.drawable.player_10
        ),
        CricketPlayerDataItem(
            "Suryakumar Yadav",
            "Suryakumar Yadav’s reputation as a T20 genius is well-established, especially during his time with the Mumbai Indians in the IPL. However, ranking him becomes a challenge due to the limited number of ODIs under his belt and his absence from Test cricket. Despite being the best T20 batsman globally, he has only played 20 ODIs, and his Test debut is yet to happen. SKY’s journey to the world stage has taken place in his 30s, partly due to being a late bloomer and partly because India delayed his international call-up. Nevertheless, his performances in the IPL and his brief stint in international cricket showcase his true potential as a 360-hitter.",
            R.drawable.player_11
        ),
        CricketPlayerDataItem(
            "Shaheen Shah Afridi",
            "Shaheen Shah Afridi has quickly ascended to become one of the best bowlers in international cricket after making his Pakistan debut at a young age. Even on relatively flat wickets in Pakistan and the UAE, Afridi can bowl with terrific velocity and produce troublesome bounce with his towering 6 feet 6-inch stature. Afridi’s versatility as a left-arm bowler extends across all three formats. In Test matches, he showcases excellent control and swing, effectively limiting scoring opportunities and keeping batsmen under pressure. In white-ball cricket, he remains a formidable force with his ability to deliver yorkers and hostile short deliveries. By posing a significant challenge for batsmen both at the start and toward the end of an innings.",
            R.drawable.player_12
        ),
        CricketPlayerDataItem(
            "Virat Kohli",
            "Critics would point out that Virat Kohli has not scored an international century in more than two years. However, India’s test match skipper continues to be a threat in all three forms of cricket, earning him a spot on our list. Kohli is placed second in One Day Internationals and tenth in Test cricket, according to the official ICC rankings. In 2022, he’ll be eager to disprove his doubters, and I’m anticipating a positive reaction from Virat Kohli this year." +
                    "This right-handed ace hitter has 43 ODI hundreds in 260 matches, putting him on track to break Sachin Tendulkar’s world record of 49 ODI hundreds. Under his helmet, he also has a massive 27 test hundreds.",
            R.drawable.player_13
        ),
        CricketPlayerDataItem(
            "Marnus Labuschagne",
            "Marnus Labuschagne of Australia created cricket history by becoming the first player to be used as a concussion substitution in a test match in 2019. It was a stroke of luck for the player as well as the Australians, who had almost by chance discovered a star batsman." +
                    "In that game, Labuschagne took the place of Steve Smith, and by the end of the year, he had surpassed his partner as the highest run-scorer in test cricket. In the longest version of the game, he continued to score heavily, and the 27-year-old will begin 2022 as the number one ranked test batter." +
                    "Labuschagne had 2644 runs with seven hundreds and a best of 215 in 35 international matches (tests and ODIs) up to and including the fourth Ashes Test at Sydney in 2022.",
            R.drawable.player_14
        ),
        CricketPlayerDataItem(
            "Dawid Malan",
            "Dawid Malan has found it difficult against Australia in the 2021/22 Ashes series, as have all of England’s test batsmen. He does, however, start the New Year as the top-ranked T20i batter, which is enough to get him on this list." +
                    "When he struck 103 against New Zealand in 2019, he became only the second English hitter to score a T20i century. He’s also made a 99 in this format for his country since then. Malan has been criticized for not scoring quickly enough in limited-overs cricket, but he should still be a key player for England in 2022.",
            R.drawable.player_15
        ),
        CricketPlayerDataItem(
            "Rohit Sharma",
            "Rohit Sharma is ready to captain India in limited-overs cricket, and we should expect to see a lot more of him in white-ball cricket. He hasn’t played in India’s test series against South Africa in 2022, but he still has a lot to contribute in the lengthier format of the game. He is a record-breaker in one-day international cricket, having scored three double hundreds in the 50-over format, including the highest total of 264. Those are great numbers, and I expect him to be much more productive in 2022." +
                    "In an ODI match, Rohit Sharma has made it a custom to stay on the 22-yard wicket for the entire 50 overs. He is the first and only player in 50-over cricket history to score three double-hundreds, two vs Sri Lanka one was vs Australia.",
            R.drawable.player_16
        ),
        CricketPlayerDataItem(
            "Kane Williamson",
            "Kane Williamson, the captain of New Zealand, has been one of the most reliable batsmen in international cricket for quite some time. In 2022, the Kiwi is predicted to do big things, and he still holds several high-ranking positions in all formats of the game." +
                    "Williamson was placed 3rd in the ICC test ratings at the start of 2022, behind Marnus Labuschagne and Joe Root. He’s also ranked seventh in the ODI rankings. Overall, New Zealand’s current skipper has almost 15,000 international runs to his credit, with 37 centuries and the best score of 251.",
            R.drawable.player_17
        ),
        CricketPlayerDataItem(
            "David Warner",
            "In 2019, Australia’s powerful left-handed opener struggled in England, but not in the most recent Ashes series. David Warner was the third-highest scorer overall in the first four tests of 2021/22, with 273 runs at an average of 45.5. Warner has been a successful batter throughout his career, and I expect him to have another productive year. He was ranked sixth in test cricket and seventh in ODIs at the start of 2022. In 2021, he received a major setback when Sunrisers Hyderabad dropped him from the IPL, but he is expected to bounce back with a new team.",
            R.drawable.player_18
        ),
        CricketPlayerDataItem(
            "Aaron Finch",
            "Aaron Finch, Australia’s limited-overs captain, has played test cricket but is primarily regarded as a white-ball specialist. He was ranked fifth in ODIs and sixth in T20Is at the start of 2022, according to the ICC ratings. Finch’s notable accomplishments include having two of the top three T20 international cricket scores at the start of 2022. The current world record of 172, set versus Zimbabwe in 2018, is included in that list. Aaron Finch is ranked 5th in the ICC ODI Rankings for batsmen in 2022, and 6th in the T20I rankings. Former ICC T20I batsman who scored 172 runs in a single T20I match against Zimbabwe in 2018, beating his previous record of 156 runs in just 63 balls against England in 2013. ",
            R.drawable.player_19
        ),
        CricketPlayerDataItem(
            "Steve Smith",
            "Steve Smith didn’t have the best year of his life in 2021, even by his lofty standards. He failed to hit a century in each of the first four tests against England in 2021/22, despite averaging over 100 in the Ashes series of 2019." +
                    "However, dismissing him would be a mistake. In this game, form is ephemeral, but class is permanent, and Smith has enough of it. He isn’t the most graceful player to watch, but he is extremely effective. From now on, Smith will concentrate on test cricket, and in the red-ball format, he has 7757 runs at an average of 60.60, with 27 hundreds and a best of 239.",
            R.drawable.player_20
        ),
        CricketPlayerDataItem(
            "Brett Lee",
            "Brett Lee known by his nickname ‘Binga’ is a former Australian cricketer. He was recognized as the 2nd fastest bowlers in world cricket after Shoaib Akhtar. He is an athletic fielder and useful lower-order batsman, with a batting average exceeding 20 in Test cricket. Together with Mike Hussey, he has held the record for highest 7th wicket partnership for Australia in ODIs since 2005–06 with 123.",
            R.drawable.player_21
        ),
        CricketPlayerDataItem(
            "Muralidaran",
            "Muttiah Muralitharan is a Sri Lankan cricketer who was rated the greatest Test match bowler ever. He is the first wrist-spinning off-spinner in the history of the game. Muttiah Muralitharan holds a number of world records." +
                    "He is the highest wicket takers in Test and ODI cricket. Murali took 800 in test and 534 wickets in ODI cricket. He is only player to take 10 wickets in a Test in four consecutive matches, and to take 50 or more wickets against every Test playing nation.",
            R.drawable.player_22
        ),
        CricketPlayerDataItem(
            "Virat Kohli",
            "Currently captains the India national team, Virat Kohli is one of most popular cricketers. Kohli holds several Indian batting records including;" +
                    "the fastest ODI century," +
                    "fastest batsman to 5,000 ODI runs" +
                    "the fastest to 10 ODI centuries." +
                    "Among the T20I world records held by Kohli are;" +
                    "fastest batsman to 1,000 runs," +
                    "most runs in a calendar year" +
                    "most fifties in the format." +
                    "He also holds the records of most runs in a single tournament of both the World Twenty20 and the IPL.",
            R.drawable.player_23
        ),
        CricketPlayerDataItem(
            "Chris Gayle",
            "Cool cricketer Chris Gayle is a West Indies crickter. He is well known for his huge and maximum sixes. He has been credited to fuelling West Indies’ total of 205 against Australia in the ICC T20 World Cup semifinal-1 which was the highest total of the tournament. Gayle is one of only four players who have scored two triple centuries at Test level: 317 against South Africa in 2005, and 333 against Sri Lanka in 2010.",
            R.drawable.player_24
        ),
    )

    fun getCricketPlayerByIndex(index: Int): CricketPlayerDataItem? {
        return if (index in list.indices) list[index]
        else null
    }

    fun getAllCrickpe(): List<CricketPlayerDataItem>{
        return list
    }
}