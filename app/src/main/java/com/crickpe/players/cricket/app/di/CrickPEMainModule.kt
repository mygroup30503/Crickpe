package com.crickpe.players.cricket.app.di

import com.crickpe.players.cricket.app.data.repository.CrickpeGetterRepositoryImpl
import com.crickpe.players.cricket.app.data.storage.CrickpeStorage
import com.crickpe.players.cricket.app.domain.repository.CrickpeGetterRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object CrickPEMainModule {

    @Provides
    fun provideStorage(): CrickpeStorage{
        return CrickpeStorage()
    }

    @Provides
    @Singleton
    fun provideCrickpeGetterRepository(storage: CrickpeStorage): CrickpeGetterRepository{
        return CrickpeGetterRepositoryImpl(storage)
    }
}