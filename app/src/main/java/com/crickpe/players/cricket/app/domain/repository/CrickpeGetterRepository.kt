package com.crickpe.players.cricket.app.domain.repository

import com.crickpe.players.cricket.app.data.model.CricketPlayerDataItem

interface CrickpeGetterRepository {

    fun gimmePlayerByIndex(index: Int): CricketPlayerDataItem?

    fun gimmePlayerList(): List<CricketPlayerDataItem>

}