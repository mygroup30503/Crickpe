package com.crickpe.players.cricket.app.domain.usecases

import com.crickpe.players.cricket.app.common.Resource
import com.crickpe.players.cricket.app.data.model.CricketPlayerDataItem
import com.crickpe.players.cricket.app.domain.repository.CrickpeGetterRepository
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GetCrickpePlayerUsecase @Inject constructor(
    private val repository: CrickpeGetterRepository
) {
    fun getPlayer(index: Int): Flow<Resource<CricketPlayerDataItem>> = flow{
        emit(Resource.Loading())
        val result = repository.gimmePlayerByIndex(index) ?: return@flow
        val delay = (0..2000L).random()
        delay(delay)
        emit(Resource.Success(result))
    }
}