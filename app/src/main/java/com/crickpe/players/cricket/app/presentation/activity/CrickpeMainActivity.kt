package com.crickpe.players.cricket.app.presentation.activity

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import com.crickpe.players.cricket.app.presentation.screens.main.CrickpeMainS
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CrickpeMainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CrickpeMainS()
        }
    }
}
