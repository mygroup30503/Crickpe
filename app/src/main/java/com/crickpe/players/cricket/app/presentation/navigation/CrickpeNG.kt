package com.crickpe.players.cricket.app.presentation.navigation


import androidx.compose.foundation.background
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.crickpe.players.cricket.app.presentation.screens.list.CrickpeListS
import com.crickpe.players.cricket.app.presentation.screens.menu.CrickpeMenuS
import com.crickpe.players.cricket.app.presentation.screens.player.CrickpePlayerS
import com.crickpe.players.cricket.app.presentation.utils.CrickpeR


@Composable
fun CrickpeNG(navHostController: NavHostController) {
    NavHost(
        navController = navHostController,
        startDestination = CrickpeR.MENU_SCREEN
    ) {
        composable(CrickpeR.MENU_SCREEN){
            CrickpeMenuS(navHostController)
        }
        composable(CrickpeR.LIST_SCREEN){
            CrickpeListS(navHostController)
        }
        composable(CrickpeR.PLAYER_SCREEN+"/{index}"){
            CrickpePlayerS()
        }
    }
}
