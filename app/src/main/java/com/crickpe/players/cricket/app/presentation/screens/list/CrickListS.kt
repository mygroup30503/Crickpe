package com.crickpe.players.cricket.app.presentation.screens.list

import android.graphics.Canvas
import android.graphics.Outline
import android.graphics.Paint
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ReplacementSpan
import android.view.Gravity
import android.view.View
import android.view.ViewOutlineProvider
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.annotation.Dimension
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Card
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.geometry.CornerRadius
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.BlendMode
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.Shadow
import androidx.compose.ui.graphics.StrokeJoin
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.ExperimentalTextApi
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import com.crickpe.players.cricket.app.R
import com.crickpe.players.cricket.app.common.Resource
import com.crickpe.players.cricket.app.data.model.CricketPlayerDataItem
import com.crickpe.players.cricket.app.presentation.screens.waiting.CrickpeWaitingS
import com.crickpe.players.cricket.app.presentation.utils.CrickpeR
import com.crickpe.players.cricket.app.presentation.utils.OutlineSpan
import com.crickpe.players.cricket.app.presentation.utils.OutlinedText

@Composable
fun CrickpeListS(navHostController: NavHostController) {
    val viewModel: CrickListSVM = hiltViewModel()
    val result = viewModel.result.collectAsState(initial = Resource.Loading())
    when (val state = result.value) {
        is Resource.Loading<List<CricketPlayerDataItem>> -> CrickpeWaitingS()
        is Resource.Success<List<CricketPlayerDataItem>> -> Column(
            modifier = Modifier
                .fillMaxSize()
                .verticalScroll(
                    rememberScrollState()
                )
        ) {
            state.data?.forEachIndexed { index, cricketPlayerDataItem ->
                CricketListUIItem(cricketPlayerDataItem = cricketPlayerDataItem) {
                    navHostController.navigate(CrickpeR.PLAYER_SCREEN + "/$index")
                }
            }
        }
    }
}

@OptIn(ExperimentalTextApi::class)
@Composable
fun CricketListUIItem(cricketPlayerDataItem: CricketPlayerDataItem, onClick: ()->Unit) {
    Card(
        modifier = Modifier
            .padding(8.dp)
            .fillMaxWidth()
            .height(300.dp)
            .clickable {
                onClick.invoke()
            }
    ) {
        Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.BottomCenter){
            Image(
                painter = painterResource(id = cricketPlayerDataItem.imageId),
                contentDescription = null,
                modifier = Modifier.fillMaxSize(),
                contentScale = ContentScale.Crop
            )
            OutlinedText(text = cricketPlayerDataItem.name, strokeWidth = 20f, strokeColor = Color.Black, fontSize = 32f)
        }
    }
}



