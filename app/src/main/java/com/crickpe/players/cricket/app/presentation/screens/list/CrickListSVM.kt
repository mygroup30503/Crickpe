package com.crickpe.players.cricket.app.presentation.screens.list

import androidx.lifecycle.ViewModel
import com.crickpe.players.cricket.app.domain.usecases.GetCrickpeListPlayerUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class CrickListSVM @Inject constructor(
    private val useCase: GetCrickpeListPlayerUseCase
): ViewModel() {
    val result = useCase.getList()
}