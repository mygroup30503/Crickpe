package com.crickpe.players.cricket.app.presentation.screens.main

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.crickpe.players.cricket.app.R
import com.crickpe.players.cricket.app.presentation.navigation.CrickpeNG
import com.crickpe.players.cricket.app.presentation.top_bar.CrickpeTB
import com.crickpe.players.cricket.app.presentation.utils.CrickpeR

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CrickpeMainS() {
    val navHostController = rememberNavController()
    val currentScreen = navHostController.currentBackStackEntryAsState()
    val cs = currentScreen.value?.destination?.route
    Scaffold(topBar = {
        if (cs != CrickpeR.MENU_SCREEN) CrickpeTB(navHostController = navHostController)
    }) {
        Box(
            modifier = Modifier
                .padding(it)
                .fillMaxSize(), contentAlignment = Alignment.Center
        ) {
            Image(painter = painterResource(id = R.drawable.bg_image), contentDescription = null)
            CrickpeNG(navHostController = navHostController)
        }
    }
}