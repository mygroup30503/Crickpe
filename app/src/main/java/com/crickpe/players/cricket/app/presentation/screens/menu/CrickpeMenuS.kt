package com.crickpe.players.cricket.app.presentation.screens.menu

import android.app.Activity
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.ActivityNavigator
import androidx.navigation.NavHostController
import com.crickpe.players.cricket.app.R
import com.crickpe.players.cricket.app.presentation.utils.CrickpeR

@Composable
fun CrickpeMenuS(navHostController: NavHostController) {
    val activity = (LocalContext.current as? Activity)
    Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center){
        Column(horizontalAlignment = Alignment.CenterHorizontally) {
            Image(
                painter = painterResource(id = R.drawable.btn_bp),
                contentDescription = null,
                modifier = Modifier.clickable {
                    navHostController.navigate(CrickpeR.LIST_SCREEN)
                })
            Spacer(modifier = Modifier.height(8.dp))
            Image(
                painter = painterResource(id = R.drawable.btn_exit),
                contentDescription = null,
                modifier = Modifier.clickable {
                    activity?.finish()
                })

        }
    }

}