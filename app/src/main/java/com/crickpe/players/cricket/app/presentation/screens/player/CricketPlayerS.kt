package com.crickpe.players.cricket.app.presentation.screens.player

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.crickpe.players.cricket.app.R
import com.crickpe.players.cricket.app.common.Resource
import com.crickpe.players.cricket.app.data.model.CricketPlayerDataItem
import com.crickpe.players.cricket.app.presentation.screens.waiting.CrickpeWaitingS
import com.crickpe.players.cricket.app.presentation.utils.OutlinedText

@Composable
fun CrickpePlayerS() {
    val viewModel: CricketPlayerSVM = hiltViewModel()
    val state = viewModel.result.collectAsState(initial = Resource.Loading())
    when (val result = state.value) {
        is Resource.Loading -> CrickpeWaitingS()
        is Resource.Success<CricketPlayerDataItem> -> {
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .verticalScroll(rememberScrollState()),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                OutlinedText(
                    text = result.data?.name ?: "",
                    strokeWidth = 20f,
                    strokeColor = Color.Black,
                    fontSize = 32f
                )
                Spacer(modifier = Modifier.height(16.dp))
                result.data?.imageId?.let { image ->
                    Image(
                        painter = painterResource(id = image),
                        contentDescription = null,
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(300.dp),
                        contentScale = ContentScale.Crop
                    )
                }
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                ) {
                    val sL = (result.data?.title ?: "").toSubstrings(46)
                    sL.forEach {
                        OutlinedText(
                            text = it,
                            strokeWidth = 20f,
                            strokeColor = Color.Black,
                            fontSize = 16f
                        )
                    }
                }
            }
        }
    }
}

fun String.toSubstrings(maxLength: Int): List<String> {
    val list = mutableListOf<String>()
    var currentString = ""
    var currentWord = ""

    fun addCurrentString() {
        if (currentString.isNotEmpty()) {
            list.add(currentString)
            currentString = ""
        }
    }

    this.forEach { char ->
        if (char.isWhitespace()) {
            if (currentString.length + currentWord.length + 1 <= maxLength) {
                // Если добавление текущего слова не приведет к превышению maxLength
                if (currentString.isNotEmpty()) {
                    currentString += " "
                }
                currentString += currentWord
                currentWord = ""
            } else {
                // Если добавление текущего слова приведет к превышению maxLength
                addCurrentString()
                currentString = currentWord
                currentWord = ""
            }
        } else {
            currentWord += char
        }
    }

    // Добавляем оставшиеся символы
    addCurrentString()

    // Проверяем, если оставшиеся слова не умещаются в maxLength
    if (currentWord.isNotEmpty()) {
        list.add(currentWord)
    }

    return list
}