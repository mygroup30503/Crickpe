package com.crickpe.players.cricket.app.presentation.screens.player

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.crickpe.players.cricket.app.common.Resource
import com.crickpe.players.cricket.app.data.model.CricketPlayerDataItem
import com.crickpe.players.cricket.app.domain.usecases.GetCrickpePlayerUsecase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@HiltViewModel
class CricketPlayerSVM @Inject constructor(
    private val usecase: GetCrickpePlayerUsecase,
    savedStateHandle: SavedStateHandle?
): ViewModel() {
    lateinit var result: Flow<Resource<CricketPlayerDataItem>>
    init {
        val id = savedStateHandle?.get<String>("index")?.toInt() ?: -1
        result = usecase.getPlayer(id)
    }
}