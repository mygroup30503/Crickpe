package com.crickpe.players.cricket.app.presentation.top_bar

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.crickpe.players.cricket.app.R

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CrickpeTB(navHostController: NavHostController) {
    CenterAlignedTopAppBar(title = {
        Image(
            painter = painterResource(id = R.drawable.title),
            contentDescription = null
        )
    }, navigationIcon = {
        Image(
            painter = painterResource(id = R.drawable.nav_icon),
            contentDescription = null,
            modifier = Modifier.clickable {
                navHostController.popBackStack()
            }.size(60.dp),
            contentScale = ContentScale.FillBounds)
    }, modifier = Modifier.padding(vertical = 5.dp))

}