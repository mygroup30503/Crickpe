package com.crickpe.players.cricket.app.presentation.utils

object CrickpeR {
    const val MAIN_SCREEN = "main"
    const val MENU_SCREEN = "menu"
    const val LIST_SCREEN = "list"
    const val PLAYER_SCREEN = "player"
}