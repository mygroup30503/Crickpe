package com.crickpe.players.cricket.app.presentation.utils

import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import com.crickpe.players.cricket.app.R


object Fonts {
    val font = FontFamily(Font(R.font.inter))
}