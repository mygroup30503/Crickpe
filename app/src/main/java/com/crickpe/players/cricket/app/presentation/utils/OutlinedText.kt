package com.crickpe.players.cricket.app.presentation.utils

import android.text.InputType
import android.text.Spannable
import android.text.SpannableString
import android.view.Gravity
import android.view.ViewGroup
import android.widget.TextView
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.viewinterop.AndroidView

@Composable
fun OutlinedText(text: String, strokeWidth: Float, strokeColor: Color, fontSize: Float) {
    val spannable = SpannableString(text)
    val outlineSpan = OutlineSpan(strokeColor.toArgb(), strokeWidth)
    spannable.setSpan(outlineSpan, 0, text.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

    AndroidView(
        factory = { context ->
            TextView(context).apply {
                layoutParams = ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
                textSize = fontSize
                setTextColor(Color.White.toArgb())
                gravity = Gravity.CENTER_HORIZONTAL
                setText(spannable, TextView.BufferType.SPANNABLE)
            }
        }, modifier = Modifier.fillMaxWidth().wrapContentHeight()
    )
}